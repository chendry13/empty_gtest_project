#include <gtest\gtest.h>
#include <iostream>

int main(int argc, char **argv)
{
   testing::InitGoogleTest(&argc, argv);
   int answer = RUN_ALL_TESTS();
   std::cin.ignore();

   return answer;
}
